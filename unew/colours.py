from colorama import Fore, Back, Style

END = Style.RESET_ALL

BLACK   = Fore.BLACK
RED     = Fore.RED
GREEN   = Fore.GREEN
YELLOW  = Fore.YELLOW
BLUE    = Fore.BLUE
MAGENTA = Fore.MAGENTA
CYAN    = Fore.CYAN
WHITE   = Fore.WHITE

BOLD_BLACK   = Style.BRIGHT + Fore.BLACK
BOLD_RED     = Style.BRIGHT + Fore.RED
BOLD_GREEN   = Style.BRIGHT + Fore.GREEN
BOLD_YELLOW  = Style.BRIGHT + Fore.YELLOW
BOLD_BLUE    = Style.BRIGHT + Fore.BLUE
BOLD_MAGENTA = Style.BRIGHT + Fore.MAGENTA
BOLD_CYAN    = Style.BRIGHT + Fore.CYAN
BOLD_WHITE   = Style.BRIGHT + Fore.WHITE



#   *** OLD COLOURS DEFINITIONS ***
#
# END         = "\033[0m"
# BLACK       = "\033[0;30m"
# RED         = "\033[0;31m"
# GREEN       = "\033[0;32m"
# YELLOW      = "\033[0;33m"
# BLUE        = "\033[0;34m"
# PURPLE      = "\033[0;35m"
# CYAN        = "\033[0;36m"
# WHITE       = "\033[0;37m"
#
# BOLD_BLACK  = "\033[1;30m"
# BOLD_RED    = "\033[1;31m"
# BOLD_GREEN  = "\033[1;32m"
# BOLD_YELLOW = "\033[1;33m"
# BOLD_BLUE   = "\033[1;34m"
# BOLD_PURPLE = "\033[1;35m"
# BOLD_CYAN   = "\033[1;36m"
# BOLD_WHITE  = "\033[1;37m"
