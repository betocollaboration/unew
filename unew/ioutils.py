# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
#
# Copyright (c) 2015--2018 by
#    Marco Erba <marco.erba01@universitadipavia.it>,
#    Luca Mantovani <luca.mantovani@pv.infn.it>,
#    Barbara De Palma <barbara.depalma01@universitadipavia.it>,
#    Nicola Mosco <nicola.mosco@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

from __future__ import print_function

import logging
import numpy as np

from tqdm import tqdm
from pprint import pformat

from .errors import ReplicaNumError
from .utils import all_equal
from .colours import *


#  Module-level logger.
_logger = logging.getLogger(name=__name__)



def load_files(file_names, R=1, columns=None):
    """
    Reads data files and divides them in R replica, selecting the specified
    columns (observables).
    Returns a list of R * num_files data matrices, if num_files is the number of files given in input.
    """
    
    replica = [] # List of replica.
    
    # If columns is given and has only one element we need to reshape the data
    # matrix, in order to have the same shape in every case.
    reshape = len(columns) == 1 if columns is not None else False
    
    for name in tqdm(file_names, desc="Please wait while loading data"):
        file_tmp = np.loadtxt(name, usecols=columns, dtype=np.float64)

        if file_tmp.size == 0:
            _logger.warning(BOLD_RED +
                            "File `%s' is empty: "
                            "will be ignored." + END, name)
            continue

        if file_tmp.shape[0] < R:
            raise ReplicaNumError("The number of replica must not be greater than the number of rows {} of file {}".format(file_tmp.shape[0], name))

        # This is needed to guarantee that file_tmp is a two dimensional array.
        if reshape or len(file_tmp.shape) == 1:
            file_tmp = file_tmp.reshape((file_tmp.shape[0], 1))

        replica += np.array_split(file_tmp, R, axis=0)
        
    return replica



def add_mask(data, R, nalpha, nrep):
    """
    This function returns a mesked version of the matrix data according to replica sizes in Nrep.
    """
    mask = np.full((R, nalpha, max(nrep)), False, dtype='bool')

    for r in range(R):
        mask[r, nrep[r] : , :] = True

    return np.ma.array(data, mask=mask, dtype=np.float64)



def prepare_data(replica):
    """
    Returns:
        - Data matrix with shape (R,nrmax,nalpha) where
          R is the number of replica, nrmax the maximal size of the replica,
          nalpha is the number of primary observables;
        - A dictionary containing the following parameters: R, nalpha, nrmax,
          ntot (the total number of data), nrep (a list with the replica sizes);
        - Boolean value: True if replica sizes are all equal, False otherwise.

    """
    
    nrep   = [r.shape[0] for r in replica]
    nrmax  = max(nrep)
    ntot   = sum(nrep)
    nalpha = replica[0].shape[1]
    R      = len(nrep)
    
    _logger.debug("Data parameters:\n\n%s\n",
        pformat({"R": R, "Nalpha": nalpha, "Nrmax": nrmax, "Ntot": ntot}))
    
    data = np.zeros((R,nrmax,nalpha), dtype=np.float64)  # data matrix
    
    for i in range(R):
        data[i, : nrep[i], :] = replica[i]
    
    # Mask the data array in case replica sizes are different.
    if not all_equal(nrep):
        data = add_mask(data, R, nalpha, nrep)
        
    return (data, nalpha, nrep)
