# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
#
# Copyright (c) 2015--2018 by
#    Marco Erba <marco.erba01@universitadipavia.it>,
#    Luca Mantovani <luca.mantovani@pv.infn.it>,
#    Barbara De Palma <barbara.depalma01@universitadipavia.it>,
#    Nicola Mosco <nicola.mosco@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

"""
Utility for the analysis of autocorrelations in Monte Carlo simulations
with Ulli Wolff's 'Gamma-method'.

Usage:
    unew [-v | -vv] <file>... [-bns -B BACKEND] [--dry-run]
         [-S STAU -R R -C CACHE]
         [-a PRIMARIES]
         [(-m MODULE -q FUNC... [-P PARAM...])]
         [--coffee]...
    unew [-v | -vv] -d DIR [-bns -B BACKEND] [--dry-run]
         [-S STAU -R R -C CACHE]
         [-a PRIMARIES]
         [(-m MODULE -q FUNC... [-P PARAM...])]
         [-I SEQ -r RANGE... -p PATTERN...]
         [--coffee]...
    unew [-v | -vv] -f CONFIG [-bns -B BACKEND] [--dry-run]
    unew --help
    unew --version
    unew --dry-run --coffee...

Arguments:
    <file>                          Path to a data file. Data format should be
                                    a NxN_alpha matrix, where N is the number of
                                    measurements and N_alpha is the number of
                                    observables. If different files are
                                    specified, they can have different values of
                                    N.

Options:
    -h --help                       Display this help message and exit.

    -V --version                    Print version and exit.

    -v                              Verbose. Can be supplied multiple times
                                    (max. 2) to increase verbosity level.

    --dry-run                       If option '-C' is specified generate the
                                    cache file and exit.

    -R R                            Specify the number of replica for each file.
                                    If option '-R' is specified each file is
                                    split into R-replica. [default: 1]

    -S STAU                         Guess for the ratio S of tau/tauint; if set
                                    to 0, absence of autocorrelations is
                                    assumed.  [default: 1.5]

    -d DIR                          Read replica files from directory DIR.

    -a PRIMARIES                    Choose the primaries to analyse as columns
                                    in data files. INDEXING STARTS FROM 1.

    -m MODULE                       Specify the name of a Python module or
                                    package where to find the definition of the
                                    derived quantity to analyse.

    -q FUNC                         Specify the name of the function to use as
                                    derived quantity. It has to be a function of
                                    the primary quantities as specified by the
                                    '-a' option.

    -I SEQ --indices=SEQ            Specify a sequence of indices to indentify
                                    the replica files in 'DIR' to be used. For
                                    example: SEQ=2,4,6.  The indices used are
                                    then merged with those provided as ranges
                                    with '--range'. INDEXING STARTS FROM 1.

    -r RANGE --range=RANGE          Specify a range constructor to generate the
                                    indices of the files in 'DIR' to use.  For
                                    example: RANGE=1,3 constructs the range
                                    [1,2,3], with indices starting from 1 and
                                    the last included.  This option can be given
                                    several times.  The indices used are then
                                    merged with those provided by the
                                    '--indices' option.

    -p PATTERN                      Specify a (bash style) pattern to select
                                    files in DIR. Note that if this option is
                                    chosen, then indices and ranges will refer
                                    just to files selected by the pattern.

    -P PARAM                        Specify optional parameters to be passed to
                                    the imported functions (derived quantities)
                                    as arguments. Parameters should be entered
                                    as key-value pairs: '-Pkey1=value1
                                    -Pkey2=value2 ...'.  The keys must match the
                                    names of the arguments in the function
                                    definition.

    -f CONFIG                       Read configuration from CONFIG.

    -C --cache=CACHE                Write configuration to CACHE.
    
    -n --no-plots                   Skip all the plots.
    
    -s --save-plots                 Save the generated plots in the current
                                    directory; this option is ignored if also
                                    '-n' is given.
    
    -b --batch-mode                 Run unew in batch mode: if '-s' is also
                                    present plots are saved but not shown.
    
    -B BACKEND --backend BACKEND    Select a backend for matplotlib. If a
                                    backend with graphical interface is chosen,
                                    this option may break batch mode execution.
                                    USE WITH CAUTION!

    -K --coffee                     Would you like a coffee?

"""

#-------------------------------------------------------------------------------
#   Let's import some useful modules.
#-------------------------------------------------------------------------------

# Future imports to be forward-compatible with more recent versions of Python.
from __future__ import (absolute_import, print_function, unicode_literals,
                        division, generators, with_statement)

from ._version import __version__

import sys, os, math
import docopt, logging, yaml
import colorama

from voluptuous import MultipleInvalid

# Unew modules.
from . import colours
from .session import session


#-------------------------------------------------------------------------------
#   Coffee break.
#-------------------------------------------------------------------------------

_coffee = """
                        (
                          )     (
                   ___...(-------)-....___
               .-\"\"       )    (       \"\"-.
         .-'``'|-._             )          _.-|
        /  .--.|   `\"\"---...........---\"\"`|
       /  /    |                              |
       |  |    |                              |
        \  \   |                              |
         `\ `\ |                              |
           `\ `|                              |
           _/ /\                             /
          (__/  \                           /
       _..---\"\"` \                       /`\"\"---.._
    .-'           \                       /             '-.
   :               `-.__             __.-'                 :
   :                  ) \"\"---...---\"\" (                :
    '._               `\"--...___...--\"`               _.'
      \\\"\"--..__                              __..--\"\"/
       '._     \"\"\"----.....______.....----\"\"\"     _.'
          `\"\"--..,,_____            _____,,..--\"\"`
                        `\"\"\"----\"\"\"`
"""



def get_option_names(argv):
    options = dict()
    
    options["dry_run"]    = argv.get("--dry-run", False)
    options["no_plots"]   = argv.get("--no-plots", False)
    options["save_plots"] = argv.get("--save-plots", False)
    options["batch_mode"] = argv.get("--batch-mode", False)
    options["backend"]    = argv.get("--backend", None)
    options["conf_file"]  = argv.get("-f", None)
    options["cache_file"] = argv.get("--cache", None)
    options["directory"]  = argv.get("-d", None)
    options["replica"]    = argv.get("<file>", [])
    options["indices"]    = argv.get("--indices", None)
    options["ranges"]     = argv.get("--range", [])
    options["patterns"]   = argv.get("-p", [])
    options["params"]     = argv.get("-P", [])
    options["module"]     = argv.get("-m", None)
    options["functions"]  = argv.get("-q", [])
    options["primaries"]  = argv.get("-a", None)
    options["R"]          = argv.get("-R", 1)
    options["stau"]       = argv.get("-S", 1.5)

    return options



def log_argv(argv):
    logger = logging.getLogger(name=__name__)

    if logger.isEnabledFor(logging.DEBUG):
        log_args = argv.copy()
        files    = log_args["<file>"]
        log_args["<file>"] = files[:min(len(files), 100)]
        logger.debug("Command line arguments:\n\n%s", yaml.safe_dump(log_args))



def make_fmt(colour):
    return logging.Formatter(
        colours.GREEN   + " ==> " +
        colours.MAGENTA + "%(levelname)s" +
        colours.END     + " - %(name)s - " +
        colour          + "%(message)s" +
        colours.END)



class UnewFormatter(logging.Formatter):
    formatters = {'DEFAULT': make_fmt(colours.BOLD_CYAN),
                  logging.DEBUG: make_fmt(colours.BOLD_CYAN),
                  logging.INFO: make_fmt(colours.BOLD_CYAN),
                  logging.WARNING: make_fmt(colours.BOLD_YELLOW),
                  logging.ERROR: make_fmt(colours.BOLD_RED)}
    
    def __init__(self, fmt=formatters['DEFAULT']):
        super(UnewFormatter, self).__init__(fmt=fmt)
        
    def format(self, record):
        formatter = self.formatters.get(record.levelno,
                                        self.formatters['DEFAULT'])
        return formatter.format(record)


    
def setup_logging_interface(level_num):
    default_levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    
    if "DEBUG" in os.environ:
        log_level = logging.DEBUG
    else:
        log_level = default_levels[level_num]
    
    console_handler = logging.StreamHandler()
    console_handler.setLevel(log_level)
    console_handler.setFormatter(UnewFormatter())
    
    root_logger = logging.getLogger("unew")  # Configure root logger.
    root_logger.setLevel(log_level)
    root_logger.addHandler(console_handler)
    
    return logging.getLogger(__name__)



def main():
    """
    This is the main function of UNEW.
    """
    
    colorama.init()  # Needed to initialize colorama.
    
    argv = None
    
    try:
        argv = docopt.docopt(__doc__,
            version="unew version " + __version__)
        
    except docopt.DocoptExit as e:
        print(e)

    else:  # This is executed if and only if no exceptions are raised.
        logger = setup_logging_interface(int(argv["-v"]))
            
        log_argv(argv)
        
        options = get_option_names(argv)
            
        with session(**options) as se:
            se.run()
        
        logger.debug("Execution finished without errors!")
            
    finally:  # This clause is guaranteed to be executed in all cases.
        logger = logging.getLogger(__name__)
        logger.info("Exiting...")
        logging.shutdown()
        
        colorama.deinit()  # Clean up colorama.
        
        if argv is not None and "--coffee" in argv:
            for i in range(argv["--coffee"]):
                print(_coffee, file=sys.stderr)



if __name__ == "__main__":
    main()
