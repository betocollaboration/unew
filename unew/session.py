# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
#
# Copyright (c) 2015--2018 by
#    Marco Erba <marco.erba01@universitadipavia.it>,
#    Luca Mantovani <luca.mantovani@pv.infn.it>,
#    Barbara De Palma <barbara.depalma01@universitadipavia.it>,
#    Nicola Mosco <nicola.mosco@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


from __future__ import print_function

import sys, logging
import matplotlib

from .configuration import (load_conf_from_file, UnewConfig,
                            ValidatorFile, ValidatorCmdLine)
from voluptuous import MultipleInvalid
from .ioutils import load_files, prepare_data
from .utils import partial_apply
from .analysis import PrimaryAnalysis, DerivedAnalysis
from .errors import *

#  Module-level logger.
_logger = logging.getLogger(name=__name__)

if _logger.isEnabledFor(logging.DEBUG):
    _exc_info = True
else:
    _exc_info = False
    


class BaseSession(object):
    """
    This class handles a complete session of the program.
    Main methods:
        :func: `loadData`:      loads the input data and prepares them for
                                the analysis,
        :func: `loadDerived`:   loads the module and the methods of the derived
                                quantity,
        :func: `run`:           performs the analysis computing all the quantities
                                of interest,
        :func: `makePlots`:     makes plots and histograms.

    .. note::
        For further details on the methods, see the respective descriptions.
    """

    def __init__(self,
                 conf,
                 cache_file=None,
                 dry_run=False,
                 no_plots=False,
                 save_plots=False,
                 batch_mode=False,
                 backend=None):
        """
        
        Parameters
        ----------
        conf
            Contains the parameters passed by the user.
        cache_file
            If not None tells the class to write a cache file.
        dry_run
            If True the analysis is not perfomed.
        exc_info
            If True prints the traceback of the current execution.
        """
        
        self.dry_run    = dry_run
        self.conf       = conf
        self.cache_file = cache_file
        self.no_plots   = no_plots
        self.save_plots = save_plots
        self.batch_mode = batch_mode
        self.backend    = backend
        
        if self.batch_mode and self.backend is None:
            self.backend = 'Agg'
            
        if self.backend is not None:
            matplotlib.use(self.backend)
        
        self.setup()
        
        _logger.debug("Initialized {}".format(self.__class__.__name__))

    def __enter__(self):
        _logger.debug("Entering session")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        _logger.debug("Session finished")
        if self.cache_file is not None:
            self.writeCache()
    
    def setup(self):
        pass
    
    def writeCache(self):
        """
        This method writes the cache file.
        """

        try:
            with open(self.cache_file, "w") as f:
                f.write(self.conf.dump())
        except EnvironmentError as e:
            _logger.error("%s", e, exc_info=_exc_info)
            sys.exit(1)
        else:
            _logger.info("Cache file '%s' written", self.cache_file)

    def loadData(self):
        """
        This method handles the loading of the input data.
        """
        
        if self.dry_run:
            _logger.debug("Data not loaded since we are dry-running")
            return
        
        _logger.info("Loading data...")
        
        try:
            data = load_files(self.conf.replica,
                              self.conf.R,
                              self.conf.primaries)

            (data, nalpha, nrep) = prepare_data(data)
            
            self.data = data
            self.num_obs = nalpha
            self.rep_sizes = nrep
            self.num_rep = len(nrep)
            
        except EnvironmentError as e:
            _logger.error("%s", e, exc_info=_exc_info)
            sys.exit(1)
            
        except (TypeError, ValueError) as e:
            _logger.error("Error while loading replica files: %s",
                          e, exc_info=_exc_info)
            sys.exit(1)

        except IndexError as e:
            _logger.error("Too many columns requested: %s",
                          e, exc_info=_exc_info)
            sys.exit(1)

        except ReplicaNumError as e:
            _logger.error("%s", e, exc_info=_exc_info)
            sys.exit(1)



class PrimarySession(BaseSession):
    def setup(self):
        if self.dry_run:
            return
        
        self.loadData()
        
        num_c = self.num_obs

        self.names = []
        
        if self.conf.primaries is not None:
            prim = [a+1 for a in self.conf.primaries]
        else:
            prim = [a+1 for a in range(num_c)]
        
        for c in range(num_c):
            self.names.append("primary observable n. {}".format(prim[c]))
        
        self.analysis = PrimaryAnalysis(
            self.data, self.rep_sizes, name=self.names
        )
        
    def run(self):
        if self.dry_run:
            return

        _logger.info("### RUNNING ANALYSIS FOR PRIMARY OBSERVABLES ###")
        _logger.info("Computing mean values")
        
        self.analysis.mean()
        
        _logger.info("Running Gamma-method")
        
        try:
            self.analysis.errors(stau=self.conf.stau)
        except GammaPathologicalError as e:
            _logger.error("%s", e, exc_info=_exc_info)
            sys.exit(1)
        except NoFluctuationsError as e:
            _logger.error("%s", e, exc_info=_exc_info)
            sys.exit(1)
            
        _logger.info("### ANALYSIS FINISHED ###")

        print("\n\t### RESULTS ###"
              "{}".format(self.analysis.results))
        
        if not self.no_plots:
            from .plots import PrimaryPlot
            
            args = [
                self.data, self.analysis.results, self.rep_sizes, self.names
            ]
            kwargs = {
                'save_plots': self.save_plots,
                'batch_mode': self.batch_mode
            }
            
            with PrimaryPlot(*args, **kwargs) as plot:
                plot()



class DerivedSession(BaseSession):
    def setup(self):
        if self.dry_run:
            return
        
        self.loadDerived()
        self.loadData()
        
        self.analysis = DerivedAnalysis(self.data, self.rep_sizes)
        
    def loadDerived(self):
        """
        This method imports the provided module with the derived quantity to analyse.
        """
        from importlib import import_module

        module_name = self.conf.module

        try:
            module = import_module(module_name)
        except ImportError as e:
            _logger.error("Module '%s' cannot be imported: %s",
                          module_name, e, exc_info=_exc_info)
            sys.exit(1)
        
        self.functions = []
        
        for name in self.conf.functions:
            try:
                self.functions.append(getattr(module, name))
            except AttributeError as e:
                _logger.error("Error in module '%s': %s",
                              module_name, e,
                              exc_info=_exc_info)
                sys.exit(1)
    
    def run(self):
        if self.dry_run:
            return
            
        _logger.info("### RUNNING ANALYSIS FOR DERIVED QUANTITIES ###")
        _logger.debug("Computing mean values")
        
        self.analysis.mean()
        
        self.names = []
        self.results = []
        
        for f in self.functions:
            name = "derived quantity '{}'".format(f.__name__)
            self.names.append(name)
            
            _logger.info("Analysis of {}".format(name))
            
            self.analysis.name = name
            
            try:
                self.analysis.apply(
                    partial_apply(f, args=self.conf.params),
                    name=name
                )
            except TypeError as e:
                _logger.error("%s", e, exc_info=_exc_info)
                sys.exit(1)
            
            _logger.debug("Running Gamma-method for {}".format(name))
            
            try:
                self.analysis.errors(stau=self.conf.stau)
            except GammaPathologicalError as e:
                _logger.error("%s", e, exc_info=_exc_info)
                sys.exit(1)
            except NoFluctuationsError as e:
                _logger.error("%s", e, exc_info=_exc_info)
                sys.exit(1)
            except ParamError as e:
                _logger.error("%s", e, exc_info=_exc_info)
                sys.exit(1)
            
            self.results.append(self.analysis.applied)
        
        print("\n\t### RESULTS ###")
        
        for res in self.results:
              print(res)
        
        if not self.no_plots:
            from .plots import DerivedPlot
            
            args = [self.data, self.results, self.rep_sizes, self.names]
            kwargs = {
                'save_plots': self.save_plots,
                'batch_mode': self.batch_mode
            }
            
            with DerivedPlot(*args, **kwargs) as plot:
                plot()



def session(conf_file=None,
            cache_file=None,
            dry_run=False,
            no_plots=False,
            save_plots=False,
            batch_mode=False,
            backend=None,
            **options):
    """Session configuration function.
    """
    
    if conf_file is not None:
        conf = load_conf_from_file(conf_file)
        validator = ValidatorFile(exc_info=_exc_info)
    else:
        conf = UnewConfig(**options)
        validator = ValidatorCmdLine(exc_info=_exc_info)

    try:
        conf = validator(conf)
    except MultipleInvalid as e:
        for err in e.errors:
            key   = err.path[0]
            value = getattr(conf, key)
            
            for i in range(1,len(err.path)-1):
                value = value[err.path[i]]

            _logger.error("Validation of '%s' for '%s' failed with reason: %s",
                          key, value, err.error_message,
                          exc_info=_exc_info)
        sys.exit(1)

    except ValidationError as e:
        _logger.error("%s", e, exc_info=_exc_info)
        sys.exit(1)
    
    _logger.debug("Configuration parameters:\n\n%s", conf)
    
    try:
        conf.setup()
    except NoFilesError as e:
        _logger.error("%s", e, exc_info=_exc_info)
        sys.exit(1)
    
    if dry_run:
        _logger.info("### DRY RUN ###")
    
    if conf.functions:
        se = DerivedSession(
            conf,
            cache_file=cache_file,
            dry_run=dry_run,
            no_plots=no_plots,
            save_plots=save_plots,
            batch_mode=batch_mode,
            backend=backend
        )
    else:
        se = PrimarySession(
            conf,
            cache_file=cache_file,
            dry_run=dry_run,
            no_plots=no_plots,
            save_plots=save_plots,
            batch_mode=batch_mode,
            backend=backend
        )
    
    return se
