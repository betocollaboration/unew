# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
#
# Copyright (c) 2015--2018 by
#    Marco Erba <marco.erba01@universitadipavia.it>,
#    Luca Mantovani <luca.mantovani@pv.infn.it>,
#    Barbara De Palma <barbara.depalma01@universitadipavia.it>,
#    Nicola Mosco <nicola.mosco@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

from __future__ import (print_function, generators, with_statement)
from builtins import range

import sys, os
import logging, fnmatch, yaml, re
from ast import literal_eval
from operator import itemgetter
from voluptuous import Schema, Optional, Exclusive, \
                       Object, ALLOW_EXTRA, Coerce, Any, All, \
                       MultipleInvalid, Invalid, Msg, message, \
                       Range

from .colours import *
from .errors import *


#  Module-level logger.
_logger = logging.getLogger(name=__name__)

if _logger.isEnabledFor(logging.DEBUG):
    _exc_info = True
else:
    _exc_info = False


#-------------------------------------------------------------------------------
#   All string objects are converted to str
#-------------------------------------------------------------------------------
_any_str = Coerce(str)

#-------------------------------------------------------------------------------
#   Helper functions for validation of command line input
#-------------------------------------------------------------------------------

def read_indices_cmd_line(index_string):
    if index_string is None:
        return None
        
    try:
        result = literal_eval(index_string)
    except (TypeError, ValueError):
        raise ValidationError("not an integer or a list of integers")
    except SyntaxError:
        raise Invalid("invalid format, indices "
                      "should be specified as a comma"
                      "separated list of integers")

    check_for_int = All(int, Range(min=1))

    result = Schema(Any(check_for_int, (check_for_int,)))(result)

    try:
        result = int(result)
    except TypeError:
        return set(i-1 for i in result)
    else:
        return set((result-1,))



def read_ranges_cmd_line(range_string):
    try:
        result = literal_eval(range_string)
    except (ValueError, TypeError):
        raise Invalid("not an integer or a list of integers")
    except SyntaxError:
        raise Invalid("invalid format, ranges "
                      "should be typed as "
                      "-rN or -rSTART,STOP or -rSTART,STOP,STEP")

    check_for_int = All(int, Range(min=1))

    rng_spec_val = Schema(Any(check_for_int, (check_for_int,), [check_for_int]))(result)
    
    try:
        rng_int = int(rng_spec_val)
    except TypeError as e:
        rng_spec_val = list(rng_spec_val)
        if len(rng_spec_val) > 1:
            rng_spec_val[0] -= 1
    else:
        rng_spec_val = [rng_int]

    try:
        rng = range(*rng_spec_val)
    except (ValueError, TypeError) as e:
        raise Invalid(e)

    return rng



def read_columns_cmd_line(columns_string):
    if columns_string is None:
        return None
    
    try:
        result = literal_eval(columns_string)
    except (TypeError, ValueError):
        raise Invalid("not an integer or a list of integers")
    except SyntaxError:
        raise Invalid("invalid format, primaries "
                      "should be specified as a comma "
                      "separated list of integers")

    check_for_int = All(int, Range(min=1))

    result = Schema(Any(check_for_int, (check_for_int,)))(result)

    try:
        result = int(result)
    except TypeError:
        return [i-1 for i in result]
    else:
        return [result-1]



def validate_params_cmd_line(params):
    def _param_format(fmt):
        def _format_checker(param):
            match = fmt.match(param)
            if match:
                return [match.group("key"), match.group("value")]
            else:
                raise Invalid("invalid format, parameters "
                              "should be typed as "
                              "-Pkey=value or -Pkey:value or -Pkey,value"
                              "with key an ASCII string and "
                              "value of numeric type")
        
        return _format_checker
    
    if not params:
        return None
    
    regex = re.compile(r"^(?P<key>[^\d\W]\w*)[=:,](?P<value>[\w+-.eE]+)$")
    
    params_list = Schema([_param_format(regex)])(params)
    params_dict = Schema({_any_str: Coerce(float)})(dict(params_list))
    
    return params_dict



#-------------------------------------------------------------------------------
#   Helper functions for validation of YAML configuration file input
#-------------------------------------------------------------------------------

def load_conf_from_file(cache_file):
    conf = None
    
    try:
        with open(cache_file, "r") as f:
            conf = yaml.safe_load(f)
            if not isinstance(conf, UnewConfig):
                raise YAMLFormatError("YAML file is not in the required "
                                      "format, see documentation")
    except EnvironmentError as e:
        _logger.error("%s", e, exc_info=_exc_info)
        sys.exit(1)
    except yaml.scanner.ScannerError as e:
        _logger.error("%s", e, exc_info=_exc_info)
        sys.exit(1)
    except yaml.parser.ParserError as e:
        _logger.error("%s", e, exc_info=_exc_info)
        sys.exit(1)
    except YAMLFormatError as e:
        _logger.error("%s", e, exc_info=_exc_info)
        sys.exit(1)
    else:
        _logger.debug("Cache file '%s' read", cache_file)
    
    return conf



def read_ranges_file(rng):
    try:
        try_int = int(rng)
    except TypeError:
        rng_spec = list(rng)
        if len(rng_spec) > 1:
            rng_spec[0] -= 1
        else:
            raise Invalid("not an integer or a list of integers")
    else:
        rng_spec = [try_int]

    try:
        rng = range(*rng_spec)
    except (ValueError, TypeError) as e:
        raise Invalid(e)

    return rng



def read_indices_file(indices):
    result = Schema([Coerce(int)])(indices)

    return set(i-1 for i in result)

def read_primaries_file(primaries):
    if not primaries:
        return None
    
    result = Schema([Coerce(int)])(primaries)
    
    return [i-1 for i in result]


#-------------------------------------------------------------------------------
#   Validation classes
#-------------------------------------------------------------------------------

class ValidatorBase(object):
    def __init__(self, exc_info=False):
        self.exc_info = exc_info
        self.any_str  = _any_str

        check_for_int = All(int, Range(min=1))
        self.int_schema = Schema(Any(check_for_int, [check_for_int]))

        self.schema = Schema({
            Optional("directory", default=None): Any(None, self.any_str),
            Optional("R", default=1): Coerce(int),
            Optional("stau", default=1.5): All(Coerce(float),
                Range(min=0.0, min_included=False)),
            Optional("module", default=None): Any(None, self.any_str)
        })

    def __call__(self, config):
        return type(config)(**self.schema(config.__dict__))



class ValidatorCmdLine(ValidatorBase):
    def __init__(self, exc_info=False):
        super(ValidatorCmdLine, self).__init__(exc_info=exc_info)
        
        self.schema = self.schema.extend({
            Optional("replica", default=[]): [self.any_str],
            Optional("patterns", default=[]): [self.any_str],
            Optional("indices", default=None): read_indices_cmd_line,
            Optional("ranges", default=[]): [All(self.any_str,
                read_ranges_cmd_line)],
            Optional("primaries", default=None): read_columns_cmd_line,
            Optional("params", default=[]): validate_params_cmd_line,
            Optional("functions", default=[]): [self.any_str]
        })



class ValidatorFile(ValidatorBase):
    def __init__(self, exc_info=False):
        super(ValidatorFile, self).__init__(exc_info=exc_info)

        self.schema = self.schema.extend({
            Optional("replica", default=None): Any(None, [self.any_str]),
            Optional("patterns", default=None): Any(None, [self.any_str]),
            Optional("indices", default=None): Any(None, read_indices_file),
            Optional("ranges", default=None): Any(None, [read_ranges_file]),
            Optional("primaries", default=None): Any(None, read_primaries_file),
            Optional("params", default=None): Any(None,
                {self.any_str: Coerce(float)}),
            Optional("functions", default=None): Any(None, [self.any_str])
        })

    def __call__(self, config):
        validated = super(ValidatorFile, self).__call__(config)

        if validated.directory is not None and validated.replica:
            raise ValidationError(
                "You shall not pass a directory "
                "together with files")

        if (validated.replica and
                (validated.indices is not None or validated.ranges)):
            _logger.warning(
                "Indices and ranges have not been used "
                "since files are specifed instead of directory")

        if validated.module and not validated.functions:
            raise ValidationError(
                "You shall specify at least one function name "
                "together with the field 'module'")
        elif validated.functions and not validated.module:
            raise ValidationError(
                "You shall specify the module name "
                "from which functions should be loaded")
                
        return validated



#-------------------------------------------------------------------------------
#   Configuration class
#-------------------------------------------------------------------------------

class UnewConfig(yaml.YAMLObject):
    yaml_tag = "!UnewConfig"
    yaml_loader = yaml.SafeLoader
    yaml_dumper = yaml.SafeDumper
    yaml_flow_style = False

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __repr__(self):
        return "UnewConfig(" + repr(self.__dict__) + ")"

    def __str__(self):
        return self.dump()
    
    def setup(self):
        if self.directory is not None:
            self._select_files()
    
    def dump(self):
        from copy import deepcopy
        
        config = deepcopy(self)

        if config.directory is not None:
            del config.replica

        if config.primaries is not None:
            config.primaries = [i+1 for i in config.primaries]

        if config.indices:
            config.indices = sorted(config.indices)

        ranges_tmp = []
        if config.ranges:
            for rng in config.ranges:
                if rng:
                    ranges_tmp.append([rng.start+1, rng.stop, rng.step])

        config.ranges = ranges_tmp

        return yaml.safe_dump(config, indent=4)

    def _select_files(self, exc_info=False):
        def _select_items(*indices):
            if not indices:
                return lambda seq: []
            elif len(indices) > 1:
                return itemgetter(*indices)
            else:
                return lambda seq: [seq[indices[0]]]

        def _merge_indices(indices, ranges):
            merged = set()
            
            if indices:
                merged |= set(indices)

            if ranges:
                for rng in ranges:
                    merged |= set(rng)
            
            return sorted(merged)
        
        abs_dir = os.path.expanduser(os.path.normpath(self.directory))
            
        try:
            files = os.listdir(abs_dir)
        except EnvironmentError as e:
            _logger.error("Unable to read directory: %s", e,
                          exc_info=_exc_info)
            sys.exit(1)

        if self.patterns:
            set_files = set()
            for p in self.patterns:  # Multiple patterns allowed.
                set_files |= set(fnmatch.filter(files, p))
            files = list(set_files)

        files = list(filter(os.path.isfile,
            (os.path.join(abs_dir, name) for name in files)))

        files.sort()

        indices = _merge_indices(self.indices, self.ranges)

        index_error = None
        
        if self.indices or self.ranges:
            try:
                files = _select_items(*indices)(files)
            except IndexError as e:
                index_error = str(e)
                files = []

        if not files:
            reason = "No file chosen in directory `{}'".format(self.directory)

            if len(self.patterns) == 1:
                reason += " with pattern `{}'".format(self.patterns[0])
            elif len(self.patterns) > 1:
                reason += " with patterns `{}'".format(self.patterns)

            if self.indices:
    	        reason += ", index specification `{}'".format(self.indices)

            if len(self.ranges) == 1:
                reason += ", range specification `{}'".format(self.ranges[0])
            elif len(self.ranges) > 1:
                reason += ", range specifications `{}'".format(self.ranges)

            if index_error is None:
                raise NoFilesError(reason)
            else:
                raise NoFilesError(reason + ": " + index_error)

        self.replica = files

        return self.replica
