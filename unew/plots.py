# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
#
# Copyright (c) 2015--2018 by
#    Marco Erba <marco.erba01@universitadipavia.it>,
#    Luca Mantovani <luca.mantovani@pv.infn.it>,
#    Barbara De Palma <barbara.depalma01@universitadipavia.it>,
#    Nicola Mosco <nicola.mosco@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

from __future__ import unicode_literals

import logging, math
import numpy as np

import matplotlib.pyplot as plt

from .utils import all_equal


#  Module-level logger.
_logger = logging.getLogger(name=__name__)



class Color(object):
    Black    = '#000000'
    Red      = '#cc0000'
    Green    = '#00cc00'
    Blue     = '#0000cc'
    Purple   = '#aa0099'
    DarkGray = '#6666666'
    Gray     = '#999999'



class PlotHelper(object):
    def __init__(self, t_max, w_opt, tau_int, rho_opt, save_plots=False):
        self.t_max   = t_max
        self.w_opt   = w_opt
        self.tau_int = [tau_int] * (t_max + 1)
        self.rho_opt = [rho_opt] * (t_max + 1)
        self.time    = [t for t in range(self.t_max + 1)]
        self.save_plots = save_plots
        
    def autoCorrTime(self, title, tau_int_fbb, dtau_int_fbb):
        """
        Plots the integrated autocorrelation time from 0 to t_max, which is
        the maximal number time-steps in the Gamma method.
        """

        fig = plt.figure()
        plt.xlabel("W")
        plt.ylabel("tau_int", rotation="vertical")

        plt.title(title)
        plt.plot(self.time, tau_int_fbb, "ro", marker=".", color=Color.Black)
        plt.errorbar(self.time, tau_int_fbb, yerr=dtau_int_fbb, linestyle="none")
        plt.axvline(self.w_opt, color=Color.Red)
        plt.plot(self.time, self.tau_int, ls="--", lw=1.0, color=Color.Green)
        plt.xlim(0, self.t_max)
        plt.grid(True)
        
        if self.save_plots:
            fig.savefig(title+".png", bbox_inches='tight')
        
        return fig

    def normAutoCorr(self, title, rho, drho):
        """
        Plots the normalized Gamma function, namely the autocorrelation from
        0 to t_max (the maximal number time-steps in the Gamma method).
        """

        fig = plt.figure()
        plt.xlabel("t")
        plt.ylabel("rho", rotation="horizontal")

        plt.title(title)
        plt.plot(self.time, rho, "ro", marker=".", color=Color.Black)
        plt.errorbar(self.time, rho, yerr=drho, linestyle="none")
        plt.axvline(self.w_opt, color=Color.Red)
        plt.plot(self.time, self.rho_opt, ls="--", lw=1.0, color=Color.Green)
        plt.xlim(0, self.t_max)
        plt.grid(True)
        
        if self.save_plots:
            fig.savefig(title+".png", bbox_inches='tight')
        
        return fig

    def histogram(self, title, data):
        fig = plt.figure()
        plt.title(title)
        plt.hist(data, bins=75)
        
        if self.save_plots:
            fig.savefig(title+".png", bbox_inches='tight')
        
        return fig



class BasePlot(object):
    def __init__(self, data, results, rep_sizes,
                 names, save_plots=False, batch_mode=False):
        self.data = data
        self.results = results
        self.names = names
        self.num_rep = data.shape[0]  # Number of replica.
        self.num_obs = data.shape[2]  # Number of primary observables.
        self.rep_sizes = rep_sizes
        self.rep_equal = all_equal(rep_sizes)
        self.save_plots = save_plots
        self.batch_mode = batch_mode
        
    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_value, traceback):
        plt.close('all')
    
    
    
class PrimaryPlot(BasePlot):
    def __call__(self):
        for alpha in range(self.num_obs):
            w_opt    = self.results.w_opt[alpha]
            t_max    = self.results.t_max[alpha]
            tau_int  = self.results.tau_int[alpha]
            rho_opt  = self.results.rho[alpha][w_opt]
            rho      = self.results.rho[alpha]
            drho     = self.results.drho[alpha]
            tau_fbb  = self.results.tau_int_fbb[alpha]
            dtau_fbb = self.results.dtau_int_fbb[alpha]
            qval     = self.results.qval[alpha]
            
            plot = PlotHelper(
                t_max, w_opt, tau_int, rho_opt,
                save_plots=self.save_plots
            )
            
            plot.autoCorrTime("Integrated autocorrelation time for " +
                self.names[alpha], tau_fbb, dtau_fbb)
            plot.normAutoCorr("Normalized autocorrelation for " +
                self.names[alpha], rho, drho)
            
            redata = self.data[:,:,alpha].flatten()
            
            title_primary = "Histogram of " + self.names[alpha]

            if not self.rep_equal:
                redata = redata.compressed()

            plot.histogram(title_primary, redata)
            
            if self.num_rep >= 2:
                fbr  = self.results.rep_value[:,alpha]  # fbr
                fb   = self.results.rep_mean[alpha]  # fb
                dvalue = self.results.dvalue[alpha]
                Ntot = sum(self.rep_sizes)
                Nrep = self.rep_sizes
                
                title_rep = "Histogram of replica " \
                            "for {}; Q={:.3}".format(self.names[alpha], qval)
                
                replica_dist = [(fbr[i] - fb) / (dvalue * math.sqrt(Ntot / Nrep[i] - 1)) for i in range(len(Nrep))]
                
                plot.histogram(title_rep, replica_dist)
        
        if self.batch_mode:
            plt.show(block=False)
        else:
            plt.show() # In this case we do not want to impose
                       # blocking behaviour, for example to allow interactive
                       # mode to be used.



class DerivedPlot(BasePlot):
    def __call__(self):
        for alpha in range(len(self.names)):
            w_opt    = self.results[alpha].w_opt
            t_max    = self.results[alpha].t_max
            tau_int  = self.results[alpha].tau_int
            rho_opt  = self.results[alpha].rho[w_opt]
            rho      = self.results[alpha].rho
            drho     = self.results[alpha].drho
            tau_fbb  = self.results[alpha].tau_int_fbb
            dtau_fbb = self.results[alpha].dtau_int_fbb
            qval     = self.results[alpha].qval
            
            plot = PlotHelper(
                t_max, w_opt, tau_int, rho_opt,
                save_plots=self.save_plots
            )
            
            plot.autoCorrTime("Integrated autocorrelation time for " +
                self.names[alpha], tau_fbb, dtau_fbb)
            plot.normAutoCorr("Normalized autocorrelation for " +
                self.names[alpha], rho, drho)
            
            if self.num_rep >= 2:
                fbr  = self.results[alpha].rep_value  # fbr
                fb   = self.results[alpha].rep_mean  # fb
                dvalue = self.results[alpha].dvalue
                Ntot = sum(self.rep_sizes)
                Nrep = self.rep_sizes
                
                title_rep = "Histogram of replica " \
                            "for {}; Q={:.3}".format(self.names[alpha], qval)
                
                replica_dist = [(fbr[i] - fb) / (dvalue * math.sqrt(Ntot / Nrep[i] - 1)) for i in range(self.num_rep)]
                
                plot.histogram(title_rep, replica_dist)
        
        if self.batch_mode:
            plt.show(block=False)
        else:
            plt.show() # In this case we do not want to impose
                       # blocking behaviour, for example to allow interactive
                       # mode to be used.
