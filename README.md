# __UNEW PROGRAM FOR ANALYSIS OF MONTE CARLO SIMULATIONS__ #

If you use our code for research, please cite our paper:

```
@article{DEPALMA2018,
title = "A Python program for the implementation of the Γ-method for Monte Carlo simulations",
journal = "Computer Physics Communications",
year = "2018",
issn = "0010-4655",
doi = "https://doi.org/10.1016/j.cpc.2018.07.004",
url = "http://www.sciencedirect.com/science/article/pii/S0010465518302534",
author = "Barbara De Palma and Marco Erba and Luca Mantovani and Nicola Mosco",
keywords = "Python, Monte Carlo simulations, Statistical mechanics, Autocorrelation time"
}
```

Monte Carlo (MC) simulations are nowadays an important and supportive tool for
various theoretical and experimental research areas.  A crucial aspect of Monte
Carlo simulations analysis is the accurate assessment of statistical and
systematic errors and the determination of the algorithm efficiency by means of
the computation of the autocorrelation time.


## Project scope

The UNEW project implements the *Gamma-method* algorithm for the estimation of
statistical errors in Monte Carlo simulations. This method can be applied both
to MC-simulated elementary observables and to arbitrary functions of them; one
of its relevant features is the computation of an effective integrated
autocorrelation time of these quantities, which is useful to assess the
efficiency of simulation algorithms regarding a specific observable or function.

The main purpose of the UNEW project is to provide a user-friendly interface in
an open-source environment. To this aim, the program is written in Python, an
interpreted open-source language which combines speed of execution and
comfortable coding. UNEW works on every operating system, provided the
installation of Python 2.7 or higher along with the required packages (see
below).


### Bibliography

For further details about the Gamma-method see

- U. Wolff. _Monte Carlo errors with less errors_, Comput. Phys. Commun. 156 (2004) 143, [arXiv:hep-lat/0306017](http://arxiv.org/abs/hep-lat/0306017).
- B. De Palma, M. Erba, L. Mantovani, and N. Mosco. _A Python program for the implementation of the Gamma-method for Monte Carlo simulations_. Comput. Phys. Commun. (2018), [arXiv:1703.02766](http://arxiv.org/abs/1703.02766).



## Installation

The UNEW project is organized as a Python package. The installation process
requires the module `setuptools` to perform the installation and the dependency
resolution.

To install the package you need to run the following command:

    $ pip install .

If `pip` is not installed in your system you can run this command
instead:

    $ python setup.py install

If you do not have administrator privileges or you prefer to install the package
locally, you can do so by running the installation command with the option
`--user`:

    $ pip install --user .

The location of the user installation depends on the platform and we refer to
the [official documentation][pyalt] for the details about the alternate ways of
installation.

See the section [Usage](#usage) to have more informations on how to run the
program.



## Technical description

The UNEW project is organized as a Python package named `unew` consisting in the
following modules.

- `ioutils`: Defines the functions `load_files` and `prepare_data` to load data
  and to prepare them in a suitable format for the analysis.  The function
  `load_files` simply loads the files from a list of pathnames, while the
  function `prepare_data` creates a multi-dimensional array with dimensions `(R,
  nrmax, N_alpha)`, where `R` is the number of replica, `nrmax` is the number of
  elements of the biggest replicum and `N_alpha` is the number of primary
  observables.

- `analysis`: This module defines all the analysis code. It defines the classes
  `PrimaryAnalysis` and `DerivedAnalysis`, which perform the analysis in the
  case the user specified respectively only primary observables or also a
  derived quantity. The results of the analysis are the mean values of primary
  observables, the expectation values of derived quantities (if specified), the
  mean deviation, the expectation values over replica and the mean over replica.
  The computation of the errors via the Gamma-method produces the following
  quantities: the unbiased estimated value of the derived quantity, along with
  its error and the error of the error; the variance of the derived quantity and
  its "naive error" computed disregarding autocorrelations; the integrated
  autocorrelation time, along with its error; the normalized autocorrelation
  function with its error and the Q-value for the histogram of replica
  distribution.

- `plots`: Defines the classes `PrimaryPlot` and `DerivedPlot`, which make plots
  of autocorrelation time, normalized autocorrelation time and histogram of
  specific observables.

- `configuration`: Performs the validation of the user input, possibly reporting
  an error if there are any mistyped parameters.

- `session`: Defines the classes `PrimarySession` and `DerivedSession`, which
  are selected depending on whether the user has specified a derived quantity or
  not.

- `utils`: Defines the function `partial_apply` used to pass optional parameters
  to the derived quantity.

- `unew`: Defines the main function which performs the analysis, using the
  modules described above.

For deeper details see the documentation of each module and the paper.


### Requirements

Python 2.7 or higher.

Required modules: docopt, numpy, scipy, matplotlib, tqdm, voluptuous, colorama.
For compatibility reasons Python 2.7 and Python 3.x, also the future package is
installed by the setup script.


### Usage

The program can be run from a terminal in three different ways, depending on how
you provide the input data files.

1. If you have few files to analyse you can directly pass them on the command
   line:

       $ unew <file1> <file2>...

2. If you prefer, you can specify a directory from where UNEW should pick the
   files:

       $ unew -d path/to/directory

3. Additionally, you can also write a configuration file with the necessary
   informations to perform the analysis:

       $ unew -f config.yaml

The program accepts also a number of options which are parsed by the Python
module `docopt`. The program usage goes as follows:
    
    unew [-v | -vv] <file>... [-bns -B BACKEND] [--dry-run]
         [-S STAU -R R -C CACHE]
         [-a PRIMARIES]
         [(-m MODULE -q FUNC... [-P PARAM...])]
         [--coffee]...
    unew [-v | -vv] -d DIR [-bns -B BACKEND] [--dry-run]
         [-S STAU -R R -C CACHE]
         [-a PRIMARIES]
         [(-m MODULE -q FUNC... [-P PARAM...])]
         [-I SEQ -r RANGE... -p PATTERN...]
         [--coffee]...
    unew [-v | -vv] -f CONFIG [-bns -B BACKEND] [--dry-run]
    unew --help
    unew --version
    unew --dry-run --coffee...
    
All the arguments in square brackets are optional parameters to customise the
execution and the analysis. In order to dispel any doubt about the execution of
the program, type on the command line:

    $ unew -h

In the section [Examples](#examples) we will give you some working examples
explaining the basic functionality of `unew`.


### How to write the configuration file

The configuration file can be generated automatically by `unew` with the option
`-C` which accepts an argument specifying the output file name. The general
structure of the input configuration file is:

```
!UnewConfig
directory: null or /path/to/DataDirectory
replica:   null or [] or [/path/to/file1, /path/to/file2, ...]
patterns:  null or [] or [pattern1, pattern2, ...]
indices:   null or [] or [1,3,4,7, ...]
ranges:    null or [] or [[1,10], [20,30], ...]
R:         integer (default 1)
stau:      float (default 1.5)
primaries: null or [] or [1,2,3, ...]
params:    null or {} or {par1: value1, par2: value2, ...}
module:    null or module_name
functions: null or [] or [func1, func2, ...]
```

Let's see briefly the meaning of the fields of the configuration file:

- `directory` is a string containing the path of the directory of simulation
  data. The default value is `null`.

- `replica` is a list of paths to data files. Note that every file is
  interpreted as a single replicum of the observables, unless the parameter `R`
  is specified (see below). The specification of `replica` excludes that of
  `directory`, and viceversa. The default value can be either an empty list or
  `null`.

- `patterns` is a list of strings containing bash-style patterns in order to
  match a specific set of files.  The default value can be either an empty list
  or `null`. (§)

- `indices` is a list of integers. This option arranges the files in the
  specified directory in alphabetical ordering (see below for explanation) and
  selects some of them according to their associated index. The default value
  can be either an empty list or `null`. (§)

- `ranges` is a list of range specifications. A Python range consists of the
  parameters `start`, `stop` and `step`. Each range specification should be a
  list with one of the following data: `[stop]`, `[start, stop]` or `[start,
  stop, step]`, where the parameter `start` starts from `1`. The default value
  can be either `null` or an empty list.

- `R` is an integer determining how many replica should be extracted from each
  file. The default value is `1`.

- `stau` is a floating-point number. This quantity refers to the first
  estimation of the errors in the gamma method (see the bibliography). Its
  default value is `1.5`.

- `primaries` is a list of integers. With this parameter you can specify which
  columns in the input files you want to analyse as primary observables.  The
  default value is `null` and column indexing starts from `1`. If `primaries` is
  `null` or an empty list, all columns in the data files are chosen.

- `module` is the name of the Python module defining the functions to be used as
  derived quantities. You can then specify the functions, which you want to use
  in the analysis, with the field `functions`. The default value is `null`.
  (§§)

- `functions` is a list containing the names of the functions defined in the
  module given through the field `module`, that shall be used as derived
  quantities for the analysis. The default value is either `null` or an empty
  list. (§§)

- `params` is a dictionary specifying the numeric parameters to be passed to the
  analysed functions. The parameters should match the corresponding ones of the
  functions defined in the module specified with `module`. The default value can
  be either `null` or an empty dictionary.

(§) These parameters are applied with different priorities by the program. The
`patterns` specification is the first to be handled and then `indices` and
`ranges` are applied with the same priority. The results of the application of
the last two parameters are joined together.

(§§) The fields `module` and `functions` must be provided together.



## Examples

Download the latest version of the UNEW tarball or clone the git repository.
Then, in a terminal window,  go to the directory with the UNEW
sources, where you can find a directory called `examples`. Here you can find
the following items:

- Two configuration files: `example_files.yaml`, `example_dir.yaml`.

- One directory `examples_data` with the analysis data.

- One Python source `ising2d.py`.

We provide two examples corresponding to the two modes of execution of UNEW,
depending on whether the user specifies a directory or a list of files.
The two examples can be run by the commands

    $ PYTHONPATH=examples unew -f examples/example_dir.yaml
    
    $ PYTHONPATH=examples unew -f examples/example_files.yaml

The definition of the environment variable `PYTHONPATH` is necessary when the
module containing the derived functions is in a different directory from the one
of the `unew` module. If you want this assignment to last for the whole session
(without specifying it at each run), you can export the variable:

    $ export PYTHONPATH=/path/to/directory
    
If you want to test the package before installing it, you can use the script
`unew-run.py` that can be found in the UNEW source directory.

The directory `examples_data` contains 10 input files filled with MC-data.
Each files contains `N=10000` measurements of `N_alpha=5` primary observables,
which are the data to be analysed in our example.

The Python module `ising2d` defines two derived quantities, `clusterE` and
`f_chi`, which can be used as functions of the primary observables; both of them
depend on the linear lattice size `L` and the lattice dimension `D` (which has
to be passed to the program via the `params` field).

The same analysis performed with the input file `example_dir.yaml` can be
performed with the following command:

    $ PYTHONPATH=examples unew -d examples/examples_data \
      -p "*.dat" -r 1,5 -I 7,9 -R 10 -S 1 \
      -P L=32 -P D=2 -m ising2d -q clusterE -q f_chi

Here we take all files in `examples_data` whose extension is `.dat`, then among
them we select the first five along with the 7th and the 9th; we split each file
into 10 replica; we set `stau=1`; we specify 32 and 2 as parameters of the
derived quantities `clusterE` and `f_chi`.

The analysis performed with the input file `example_files.yaml` is instead
equivalent to:

    $ PYTHONPATH=examples unew examples/examples_data/example_data_7.dat \
      examples/examples_data/example_data_9.dat \
      -R 10 -S 1 -P L=32 -P D=2 -m ising2d -q clusterE -q f_chi

The only difference with respect to the previous case is that we take as input
the data in the files `example_data_7.dat` and `example_data_9.dat` (each
divided into 10 replica).

In the previous examples we could have specified also the options:

- `-s` to save the plots in the current directory;
- `-b` to execute UNEW in batch mode; notice that when the `-b` option is
  specified, one needs to specify also `-s` to save the plots;
- `-n` to avoid producing the plots;
- `-B` to specify a particular backend to be used with matplotlib; for example,
  one can specify the `Agg` backend when the program is run in a non-interactive
  session. With the `-b` option the program uses by default the `Agg` backend.


[pyalt]: http://docs.python.org/3/install/index.html#alternate-installation
