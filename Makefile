#tests=$(wildcard test*.py)
package=unew
exclude=$(wildcard $(package)/__init__.*)
modules_files=$(filter-out $(exclude), $(wildcard $(package)/*.py))
modules_names=$(patsubst $(package)/%, $(package).%, $(modules_files))
#sources=$(basename $(tests) $(modules_names))
sources=$(basename $(modules_names))

all: doc clean
	
doc: $(tests) $(modules_files) $(package)
	pydoc -w $(sources) $(package)
	mkdir -p doc
	mv *.html doc/
	
clean:
	rm -f *.pyc $(package)/*.pyc
	rm -rf __pycache__ $(package)/__pycache__

purge: clean
	rm -f doc/*.html

.PHONY: all doc clean purge
