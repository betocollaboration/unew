from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()
        

setup(
    name='unew',
    version="3.0.2",
    license='MIT',
    description='MC-data analysis tool',
    long_description=readme(),
    url='http://bitbucket.org/betocollaboration/unew',
    author='Barbara De Palma, Marco Erba, Luca Mantovani, Nicola Mosco',
    author_email='betocollaboration@googlegroups.com',
    packages=['unew'],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Physics',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3'
    ],
    entry_points={
        'console_scripts': ['unew=unew.unew:main']
    },
    install_requires=[
        'numpy',
        'scipy',
        'matplotlib',
        'docopt',
        'voluptuous',
        'pyyaml',
        'tqdm',
        'colorama',
        'future'
    ]
)
