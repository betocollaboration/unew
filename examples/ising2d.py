#-------------------------------------------------------------------------------
# These module contains definitions of derived functions, to be taken as
# examples.  Each function must depend on two arguments (abb, args), meaning
# "abb" is a list of mean values of the primary observables (according to the
# order in which they are selected by the option '-a'), while "args" is a list
# of parameters given by the user.
#-------------------------------------------------------------------------------

import math
import numpy as np



# ------------------------------------------------------------------------
#               CLUSTER OBSERVABLES
# ------------------------------------------------------------------------

# This is an example of function which requires one parameter.
# It requires at least one primary variable to be selected in order to work.

def clusterE(abb,L,D):
    """
    Calculation of the mean energy:
    <E> = <E_(tot)>/Volume
    where "<>" means average over configurations (ensemble mean).
    """
    V = pow(L,D)
    return abb[0]/(V)

# This is an example of function which requires one parameter.
# It requires at least two primary variables to be selected in order to work.

def f_chi(abb,L,D):
    """
    Computation of f_chi = L^(7/4)/chi,
    where L is the linear lattice size
    and chi is the susceptivity.
    abb[3] is the mean squared magnetization and abb[4] is the mean magnetization.
    """
    V = pow(L,D)
    exp = 7.0/4.0
    return pow(L,exp)/((abb[3]/V-(abb[4]/V)**2))
