# __ChangeLog__ #

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][kcl] and this project adheres to
[Semantic Versioning][semver].

To update this file is strongly encouraged to follow the guidelines in the
above-mentioned articles and to read the notes about [GitHub Markdown][gitmd].


## [Unreleased]

### Added

### Changed

### Fixed



## [3.0.2] | 2018-10-03

### Fixed

- Fixed error arising when using unew with Python 2.7 in validation of
  parameters. In function `validate_params_cmd_line` we removed `re.ASCII` which
  is not available in Python 2.7 (strings in Python 2.7 are not unicode
  already). In order to have a better error message also in higher Python
  versions we modified the message specifying that `key` should be an ASCII
  encoded string.



## [3.0.1] | 2018-07-24

### Added

- Added citation suggestion to published paper and modified the bibliographic
  information accordingly.



## [3.0.0] | 2018-06-30

### Added

- Added option '-B' to select the backend used by matplotlib.

### Changed

- Improved `unew` help message.
- Added description for the '-B' option.

### Fixed

- Fixed error handling when reading from file, now missing module or functions
  when the other missing produces consistent error messages.
- Updated links in ChangeLog.
- Fixed example cache files with consistent parameters as in README.md.
- Fixed issue when executing `unew` in batch mode. Now the program automatically
  select non-GUI backend if it runs in batch mode, unless a GUI backend has been
  explicitly chosen with the '-B' option.



## [2.2.0] | 2018-06-30

### Changed

- Reading from cache file: now also the field primaries can be an empty list.
- Dependency on the future package for compatibility between Python 2.7 and
  Python 3.x.
- Updated year in copyright clause.
- Updated Development Status classifier in the setup script, we are now in   
  production!

### Fixed

- Fixed error messages for voluptuous for primaries, indices and ranges.
- range is now builtins.range (from the future package).
- Fixed dump function of UnewConfig in the case of ranges.
- Fixed reading primaries and ranges from file: indices start from 1 not 0.



## [2.1.3] | 2018-02-14

### Changed

- Updated Makefile.



## [2.1.2] | 2018-02-14

### Changed

- Updated ChangeLog file.



## [2.1.0] | 2017-12-08

### Fixed

- Fixed bugs for configuration validation.
- Fixed validation of command line options.



## [2.0.4] | 2017-07-19

### Changed

- Improved output of results.


### Fixed

- Fixed computation of Q value.



## [2.0.3] | 2017-05-12

### Changed

- When data exhibit no fluctuations, an error is raised instead of simply
  issuing a warning message.
- Option `--range` now accepts also a range in the form `-r[...]`.
- Checking of derived functions in a module is now performed before loading data
  by the method `setup` that has to be overridden by derived classes.


### Fixed

- `GammaPathologicalError` now imported correctly in `analysis.py`.
- `MultipleInvalid` now imported correctly in `session.py`.



## [2.0.2] | 2017-03-07



## [2.0.1] | 2017-03-03

### Fixed

- Fixed examples module `derived.py`.



## [2.0.0] | 2017-03-03

### Added

- Added options `-b`, `-n`, `-s`.



## [2.0.0b0] | 2017-02-10

Preparing for the new release version `2.0.0`.


## [1.10.0] | 2017-02-10

### Added

- Added file `ChangeLog.md`. Now you can follow the timeline of the changes in
  the project across different releases.
  
- Added a new module `configuration.py` with all the functions and classes
  related to the validation of user input through both the command line and the
  cache file.


### Changed

- Now serialisation is done using YAML. This breaks compatibility with pure JSON
  configuration files. Here is an example with a reference to the class
  `UnewConfig` with a YAML tag:  
```YAML
!UnewConfig
R: 1
functions: null
directory: path/to/data
indices: null
module: null
params: null
patterns: null
primaries: null
ranges: null
stau: 1.5
```

- Changed class `Conf` to `UnewConfig` which is now a subclass of `YAMLObject`.

- Changed defaults when loading a configuration from file: fields that are
  interpreted as Python lists can either be empty lists or can have value
  `null`.

- Updated `README.md` accordingly to take into account the transition from JSON
  to YAML.

- Dropped use of pformat in favor of YAML: printed output now looks the same as
  the representation in the cache.  Similarly, command line arguments are
  printed with `yaml.safe_dump`.

- Renamed field `pattern` as `patterns`.

- Changed name of function `make_data` to `prepare_data`.

- Changed name of function `make_mask` to `add_mask`.

- Now option `-P` on the command line can be repeated and parameters can be
  specified in the following ways:

    `-Pkey=val` or `-Pkey:val` or `-Pkey,val`.

- Renamed field `derived` as `functions`.


### Fixed

- Fixed YAML support and validation of ranges, indices and primaries.

- Fixed title in README.md as a Markdown header.

- Fixed issues with the validation of primaries and ranges.



## [1.9.0] | 2017-01-03

### Fixed

- Fixed computation of bias in gamma and output in main when printing results.

- Fixed arguments in example module for aloop and chi.



## [1.9.0-beta] | 2016-12-20

### Added

- Added comments to modules `gamma.py`, `obs.py`, `ioutils.py`.


### Changed

- Renamed module `uwerr` as `unew_mod`.


### Fixed

- Modified file `Makefile` generating the documentation, since the library name
  has been changed to `unew_mod`.



## [1.0] | 2016-02-19

### Added

- Improved handling of binary and unicode strings under Python 2.7 and 3.x.

- Now UNEW shows also histograms of the input data.

- Added module `colours` for coloured printing.


### Fixed

- Fixed error handling with variable `exc_info`: now traceback is shown only in
  debug mode (with option `-vv`).
  
- Add check for the number of replica in `load_files` for consistency with the
  number of rows in each data file.



[kcl]: http://keepachangelog.com/
[gitmd]: http://guides.github.com/features/mastering-markdown/
[semver]: http://semver.org/


[Unreleased]: http://bitbucket.org/betocollaboration/unew/compare/develop..v3.0.2
[3.0.2]: http://bitbucket.org/betocollaboration/unew/compare/v3.0.2..v3.0.1
[3.0.1]: http://bitbucket.org/betocollaboration/unew/compare/v3.0.1..v3.0.0
[3.0.0]: http://bitbucket.org/betocollaboration/unew/compare/v3.0.0..v2.2.0
[2.2.0]: http://bitbucket.org/betocollaboration/unew/compare/v2.2.0..v2.1.3
[2.1.3]: http://bitbucket.org/betocollaboration/unew/compare/v2.1.3..v2.1.2
[2.1.2]: http://bitbucket.org/betocollaboration/unew/compare/v2.1.2..v2.1.0
[2.1.0]: http://bitbucket.org/betocollaboration/unew/compare/v2.1.0..v2.0.4
[2.0.4]: http://bitbucket.org/betocollaboration/unew/compare/v2.0.4..v2.0.3
[2.0.3]: http://bitbucket.org/betocollaboration/unew/compare/v2.0.3..v2.0.2
[2.0.2]: http://bitbucket.org/betocollaboration/unew/compare/v2.0.2..v2.0.1
[2.0.1]: http://bitbucket.org/betocollaboration/unew/compare/v2.0.1..v2.0.0
[2.0.0]: http://bitbucket.org/betocollaboration/unew/compare/v2.0.0..v2.0.0b0
[2.0.0b0]: http://bitbucket.org/betocollaboration/unew/compare/v2.0.0b0..v1.10.0
[1.10.0]: http://bitbucket.org/betocollaboration/unew/compare/v1.10.0..v1.9.0
[1.9.0]: http://bitbucket.org/betocollaboration/unew/compare/v1.9.0..v1.9.0-beta
[1.9.0-beta]: http://bitbucket.org/betocollaboration/unew/compare/v1.9.0-beta..v1.0
[1.0]: http://bitbucket.org/betocollaboration/unew/compare/v1.0..v0.1.0-beta
